<h1> Git Flow </h1>

- Branchs principais

<p> Primeiramente trabalhamos com 3 (em alguns lugarres 2 como na figura abaixo) branchs principais. Master (atualmente muitos repositórios a chamam de main, mas é a mesma coisa), Develop e QA (ou homologação). Alguns lugares usam uma branch só para Develop e QA. Essas branchs não devem ser excluidas. </p>

<p> Essas branchs são protegidas e não devem receber em hipótese alguma commits diretos. Qualquer alteração nelas deve vir por meio de PRs(Pull Requests) aprovados. </p>

![Master develop](images/gitflow1.svg "Master develop")

- Feature

<p> Para novas atualizações fazemos commits diretamente em uma feature branch, que nascem a partir da develop. Quando estão prontas são novamente mergeadas na develop e posteriormente em qa, e com tudo aprovado e testado, pode ser feito merge para master com dia e horário apropriados para o deploy marcados. Após feito o merge da feature branch, ela deve ser excluida. </p>

![Feature](images/gitflow2.svg "Feature")

- Release

<p> Para melhor organização, facilidade para rollback e versionamento, as Versões que entram em produção são chamadas de releases. Normalmente como x.y.z-w, tal que x indica uma mudança do tipo major, y do tipo medium, z do tipo minor e w o numero de builds dessa versao. Normalmente não usamos o numero do build no versionamento da release. </p>

![Release](images/gitflow3.svg "Release")

-Hotfix

<p> Por fim e não menos importante, branchs do tipo hotfix, são as únicas que não seguem o fluxo normal, se originam da master pra que seja feita uma correção rápida, todas as validações são feitas na própria branch, e ela é mergeada diretamente na master. </p>
<p> Obs. É importante ressaltar que ao ser mergeado na master, a master deve ser mergeada em qa e na master, para que na próxima release não sejam perdidas as alterações do hotfix. </p>
<p> Obs2. Após feito merge a branch hotfix deve ser excluída. </p>

![Hotfix](images/gitflow4.svg "Hotfix")
